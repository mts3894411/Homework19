package reqresin;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.UserDTO;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetTest {
    public GetTest() throws URISyntaxException, IOException, InterruptedException {
    }

    @Test
    @DisplayName("Получение списка пользователей методом GET/api/users?page=2")
    public void successGetListUsers() throws IOException, URISyntaxException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users?page=2"))
                .GET().build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
        assertEquals(200, response.statusCode());

    }

    @Test
    @DisplayName("Получение несуществующего пользователя методом GET/api/unknown/2")
    public void GetSingleUserNotFound() throws IOException, InterruptedException, URISyntaxException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/23"))
                .GET().build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(HttpStatus.SC_NOT_FOUND, response.statusCode());
    }

    ObjectMapper mapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    @Test
    @DisplayName("Получение и валидация одного пользователя методом GET/singleUser")
    public void successGetSingleUser() throws IOException, InterruptedException, URISyntaxException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/2"))
                .GET().build();

        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        String responseBody = response.body();
        //System.out.println(responseBody);
        UserDTO user = mapper.readerFor(UserDTO.class).readValue(responseBody);
        //System.out.println(user);
        assertAll(
                "User",
                () -> assertEquals(200, response.statusCode()),
                () -> assertEquals(2, user.getId()),
                () -> assertEquals("Janet", user.getFirst_name()),
                () -> assertEquals("Weaver", user.getLast_name()),
                () -> assertEquals("https://reqres.in/img/faces/2-image.jpg", user.getAvatar())
        );

    }

}
