package reqresin;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.CreateDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteTests {
    static int userIdToDelete;
    @BeforeEach
    @DisplayName("Предусловие с созданием пользователя")
    public void initUser() throws URISyntaxException, IOException, InterruptedException {
        String out = "{\"name\":\"morpheus\",\"job\":\"leader\"}";
        HttpRequest request = HttpRequest.newBuilder()
                //чтобы сервер правильно считал информацию, нужно указать тип контента - JSON:
                .header("Content-Type", "application/json")
                .uri(new URI("https://reqres.in/api/users"))
                .POST(HttpRequest.BodyPublishers.ofString(out))
                .build();// Запрос
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        String responseBody = response.body();
        CreateDTO user = new ObjectMapper().readerFor(CreateDTO.class).readValue(responseBody);
        System.out.println(response.body());
        userIdToDelete = user.getId();
        System.out.println(userIdToDelete);
    }
    @Test
    @DisplayName("Удаление пользователя методом DELETE/api/users/2")
    public void DeleteDelete() throws IOException, URISyntaxException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/2"))
//                В данном случае используем id=2, т.к. сервер reqres.in не запоминает id, созданного пользователя
//                .uri(new URI("https://reqres.in/api/users/"+userIdToDelete));
                .DELETE().build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        assertEquals(204, response.statusCode());
    }
}

