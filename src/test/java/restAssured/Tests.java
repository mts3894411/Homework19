package restAssured;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.LoginDTO;
import dto.RegisterDTO;
import dto.UserDTO;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Tests {
    private static RequestSpecification spec = new RequestSpecBuilder()
            .setContentType(ContentType.JSON)
            .setBaseUri("https://reqres.in/api/")
            .build();
    ObjectMapper mapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    @Test
    @DisplayName("Получение одного пользователя методом GET/singleUser")
    public void successGetSingleUser() throws JsonProcessingException {
        String responseBody = given()
                .spec(spec)
                .pathParam("id", 2)
                .when()
                .get("users/{id}")
                .then()
                .statusCode(200)
                .extract().asString();
        UserDTO user = mapper.readerFor(UserDTO.class).readValue(responseBody);
        System.out.println(responseBody);
        assertAll(
                "User",
                () -> assertEquals(2, user.getId()),
                () -> assertEquals("Janet", user.getFirst_name()),
                () -> assertEquals("Weaver", user.getLast_name()),
                () -> assertEquals("https://reqres.in/img/faces/2-image.jpg", user.getAvatar()));

    }
    @Test
    @DisplayName("Обновление пользователя методом PATCH/users/2")
    public void patchUpdate(){
        String responseBody = given()
                .spec(spec)
                .body(new LoginDTO("morpheus","zion resident" ))
                .pathParam("id", 2)
                .when()
                .patch("users/{id}")
                .then()
                .statusCode(200)
                .extract().asString();
        System.out.println(responseBody);
    }
    @Test
    @DisplayName("Обновление пользователя методом PUT/users/2")
    public void putUpdate(){
        String responseBody = given()
                .spec(spec)
                .body(new LoginDTO("morpheus","leader" ))
                .pathParam("id", 2)
                .when()
                .put("users/{id}")
                .then()
                .statusCode(200)
                .extract().asString();
        System.out.println(responseBody);
    }
    @Test
    @DisplayName("Регистрация пользователя методом POST/api/register")
    public void successPostRegister(){
        String responseBody = given()
                .spec(spec)
                .body(new RegisterDTO("eve.holt@reqres.in","pistol" ))
                .when()
                .post("register")
                .then()
                .statusCode(200)
                .extract().asString();
        System.out.println(responseBody);
    }

}
