package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class UserDTO {
    int id;
    String email;
    String first_name;
    String last_name;
    String avatar;
    @JsonProperty("data")//Пишем это, чтобы выводилась только "data", а "support" не выводился
    private void unpackNested(Map<String, Object> data) {
        this.id = (int) data.get("id");
        this.email = (String) data.get("email");
        this.first_name = (String) data.get("first_name");
        this.last_name = (String) data.get("last_name");
        this.avatar = (String) data.get("avatar");
    }
}
