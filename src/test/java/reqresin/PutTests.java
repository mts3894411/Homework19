package reqresin;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class PutTests {
    @Test
    @DisplayName("Обновление пользователя методом PUT/api/users/2")
    public void PutUpdate() throws URISyntaxException, IOException, InterruptedException {
        String out = "{\"name\":\"morpheus\",\"job\":\"leader\"}";
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                //чтобы сервер правильно считал информацию, нужно указать тип контента - JSON
                .uri(new URI("https://reqres.in/api/users/2"))
                .PUT(HttpRequest.BodyPublishers.ofString(out)).build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        String responseBody = response.body();
        System.out.println(responseBody);
//        Если не написать header, то в responseBody нет имени и работы
//       Ответ: {"updatedAt":"2023-03-15T22:49:41.243Z"}
    }

}
