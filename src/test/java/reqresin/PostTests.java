package reqresin;


import com.fasterxml.jackson.databind.ObjectMapper;
import dto.RegisterDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

public class PostTests {
    @Test
    @DisplayName("Создание нового пользователя методом POST/api/users")
    public void successPostCreateUser() throws IOException, URISyntaxException, InterruptedException {
        byte[] out = "{\"name\":\"aida\",\"job\":\"thinker\"}".getBytes(StandardCharsets.UTF_8);
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                //чтобы сервер правильно считал информацию, нужно указать тип контента - JSON
                .uri(new URI("https://reqres.in/api/users"))
                .POST(HttpRequest.BodyPublishers.ofByteArray(out))
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        String responseBody = response.body();
        System.out.println(responseBody);
        // Если не написать header, то в responseBody нет имени и работы
//       Ответ: {"id":"","createdAt":""}
    }

    @Test
    @DisplayName("Регистрация пользователя методом POST/api/register")
    public void successPostRegister() throws URISyntaxException, IOException, InterruptedException {
        RegisterDTO registerDTO = new RegisterDTO("eve.holt@reqres.in", "pistol");
        String out = new ObjectMapper().writeValueAsString(registerDTO);
        URI uri = new URI("https://reqres.in/api/register");
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                //чтобы сервер правильно считал информацию, нужно указать тип контента - JSON
                .uri(uri)
                .POST(HttpRequest.BodyPublishers.ofString(out))
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        String responseBody = response.body();
        System.out.println(responseBody);
//  Если не написать header, то выдает ошибку: 400
//Ответ: {"error":"Missing email or username"}
    }

}
