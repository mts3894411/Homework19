package dto;

import lombok.Data;

@Data
public class CreateDTO {
    String name;
    String job;
    int id;
    String createdAt;
}
